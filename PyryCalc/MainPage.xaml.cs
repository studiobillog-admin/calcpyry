﻿using System;
using System.Data;
using Xamarin.Forms;

namespace PyryCalc
{
    public partial class MainPage : ContentPage
    {
        bool canTypeDot = true;
        public MainPage()
        {
            InitializeComponent();
            AddDigitClickedEvents();
        }

        private void AddDigitClickedEvents()
        {
            Digit0.Clicked += (s, e) => AddCharToTextResult('0');
            Digit1.Clicked += (s, e) => AddCharToTextResult('1');
            Digit2.Clicked += (s, e) => AddCharToTextResult('2');
            Digit3.Clicked += (s, e) => AddCharToTextResult('3');
            Digit4.Clicked += (s, e) => AddCharToTextResult('4');
            Digit5.Clicked += (s, e) => AddCharToTextResult('5');
            Digit6.Clicked += (s, e) => AddCharToTextResult('6');
            Digit7.Clicked += (s, e) => AddCharToTextResult('7');
            Digit8.Clicked += (s, e) => AddCharToTextResult('8');
            Digit9.Clicked += (s, e) => AddCharToTextResult('9');
        }

        private void AddCharToTextResult(char charToAdd)
        {
            ResultLabel.Text += charToAdd;
        }

        private void DotClicked(object sender, EventArgs e)
        {
            if (IsResultEmpty())
            {
                return;
            }

            if (!IsLastCharSign() && canTypeDot)
            {
                canTypeDot = false;
                AddCharToTextResult('.');
            }
        }

        private void PlusClicked(object sender, EventArgs e)
        {
            if (IsLastCharSign())
            {
                return;
            }

            if (IsResultEmpty())
            {
                DisplayAlert("Znak nie może być na początku!");
                return;
            }

            canTypeDot = true;
            AddCharToTextResult('+');
        }
        private void MinusClicked(object sender, EventArgs e)
        {
            if (IsResultEmpty() || !IsLastCharSign() || IsLastCharEqual('/'))
            {
                canTypeDot = true;
                AddCharToTextResult('-');
            }

        }
        private bool IsLastCharEqual(char c)
        {
            return ResultLabel.Text[ResultLabel.Text.Length - 1] == c;
        }

        private void DivideClicked(object sender, EventArgs e)
        {
            if (IsLastCharSign())
            {
                return;
            }

            if (IsResultEmpty() || IsLastCharSign())
            {
                DisplayAlert("Znak nie może być na początku!");
                return;
            }
            canTypeDot = true;
            AddCharToTextResult('/');
        }

        private void MultipleClicked(object sender, EventArgs e)
        {
            if (IsLastCharSign())
            {
                return;
            }
            else if (IsResultEmpty())
            {
                DisplayAlert("Znak nie może być na początku!");
                return;
            }

            canTypeDot = true;
            AddCharToTextResult('*');
        }
        private void ClearClicked(object sender, EventArgs e)
        {
            ResultLabel.Text = "";
        }
        private void EqualClicked(object sender, EventArgs e)
        {
            if (IsLastCharSign())
            {
                DisplayAlert("Błędne działanie");
            }
            else if (IsDividedByZero())
            {
                DisplayAlert("Pamiętaj cholero nie dziel przez zero! :)");
            }
            else
            {
                DisplayResult();
            }
        }

        private void DisplayResult()
        {
            var result = double.Parse(new DataTable().Compute(ResultLabel.Text, "").ToString());
            ResultLabel.Text = Math.Round(result, 2).ToString();
        }

        private void DisplayAlert(string message)
        {
            App.Current.MainPage.DisplayAlert("Źle wpisałeś wariacie!", message, "Zrozumiano!");
        }

        private bool IsResultEmpty()
        {
            return ResultLabel.Text.Length == 0;
        }

        private bool IsLastCharSign()
        {
            string resultString = ResultLabel.Text;

            if (resultString.Length == 0)
            {
                return true;
            }

            if (resultString[resultString.Length - 1] == '+')
            {
                return true;
            }
            else if (resultString[resultString.Length - 1] == '-')
            {
                return true;
            }
            else if (resultString[resultString.Length - 1] == '/')
            {
                return true;
            }
            else if (resultString[resultString.Length - 1] == '*')
            {
                return true;
            }
            else if (resultString[resultString.Length - 1] == '.')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool IsDividedByZero()
        {
            string resultText = ResultLabel.Text;

            for (int i = 0; i < resultText.Length - 1; i++)
            {
                if (i < resultText.Length - 4)
                {
                    if (resultText[i] == '/' 
                        && resultText[i + 1] == '-' 
                        && resultText[i + 2] == '0' 
                        && resultText[i + 3] == '.' 
                        && resultText[i + 4] == '0')
                    {
                        return true;
                    }
                }
                if (i < resultText.Length - 3)
                {
                    if (resultText[i] == '/' 
                        && resultText[i + 1] == '-' 
                        && resultText[i + 2] == '0' 
                        && resultText[i + 3] == '.')
                    {
                        return false;
                    }
                    if (resultText[i] == '/' 
                        && resultText[i + 1] == '0' 
                        && resultText[i + 2] == '.' 
                        && resultText[i + 3] == '0')
                    {
                        return true;
                    }
                }
                if (i < resultText.Length - 2)
                {
                    if (resultText[i] == '/' 
                        && resultText[i + 1] == '-' 
                        && resultText[i + 2] == '0')
                    {
                        return true;
                    }
                    if (resultText[i] == '/' 
                        && resultText[i + 1] == '0' 
                        && resultText[i + 2] == '.')
                    {
                        return false;
                    }
                }
                if (resultText[i] == '/' && resultText[i + 1] == '0')
                {
                    return true;
                }
            }

            return false;
        }
    }
}
